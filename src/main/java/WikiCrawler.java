import java.io.IOException;
import java.util.Stack;
import java.util.stream.Collectors;

public class WikiCrawler extends Crawler {
    @Override
    protected Page visit(String link) {
        WikiPage wikiPage = null;
        if (!this.visitedLinks.contains(link)) {
            try {
                wikiPage = new WikiPage(link);
            } catch (IOException e) {
                System.out.println("could not connect to:" + link);
            }
        }
        this.visitedLinks.add(link);
        return wikiPage;
    }

    @Override
    public void crawl(String startLink, int maxPages) {
        int visited = 0;
        Stack<String> unvisitedUrls = new Stack<>();
        unvisitedUrls.add(startLink);
        while (visited < maxPages && !unvisitedUrls.isEmpty()) {
            final String curLink = unvisitedUrls.pop();
            if (!this.visitedLinks.contains(curLink)) {
                visited++;
                Page page = this.visit(curLink);
                if (page != null) {
                    unvisitedUrls.addAll(page.getTravelLinks().stream().collect(Collectors.toList()));
                    System.out.println(page);
                }
            }
        }
    }
}
