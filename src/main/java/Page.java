import lombok.Data;

import java.util.Set;

@Data
public abstract class Page {
    private String title;
    private String forwardingLink;
    private int numberOfImages;
    private int numberOfWords;
    private int numberOfChars;
    private int numberOfLinks;
    private Set<String> travelLinks;
}
