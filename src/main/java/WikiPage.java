import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WikiPage extends Page {
    public WikiPage(String url) throws IOException {
        final Document document = Jsoup.connect(url).get();
        this.setTitle(document.title());

        this.setForwardingLink(url);

        this.setNumberOfChars(document.text().toCharArray().length);

        final String[] words = document.text().split("([\\W\\s]+)");
        this.setNumberOfWords(words.length);

        final Elements allLinks = document.select("a[href]");
        Stream<String> links = allLinks.stream()
                .map(link -> link.attributes().get("href"))
                .filter(link -> link.matches(".*https://.*.wikipedia.*"));
        this.setTravelLinks(links.collect(Collectors.toSet()));
        this.setNumberOfLinks(allLinks.size());

        final Elements images = document.getElementsByTag("img");
        this.setNumberOfImages(images.size());
    }
}
