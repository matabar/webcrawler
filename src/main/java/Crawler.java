import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

public abstract class Crawler {
    @Getter
    private final long startTime;
    protected Set<String> visitedLinks;

    protected Crawler() {
        this.startTime = System.currentTimeMillis();
        this.visitedLinks = new HashSet<>();
    }

    protected abstract Page visit(String link);
    public abstract void crawl(String startLink, int maxPages);
}
